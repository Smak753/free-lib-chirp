package com.example.encryption;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.quietmodem.Quiet.FrameTransmitter;
import org.quietmodem.Quiet.FrameTransmitterConfig;
import org.quietmodem.Quiet.ModemException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity {
    private FrameTransmitter transmitter;
    private Spinner profileSpinner;
    private byte[] message=null;
    private Key privateKey;
    private String publicKeyRSA;
    private  String AESKey;
    private ArrayAdapter<String> spinnerArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        publicKeyRSA = RSAKey("public");//Получение статичного ключа RSA public
        String privateKeyRSA = RSAKey("private");//Получение статичного ключа RSA private
        findViewById(R.id.take).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Get.class));
            }
        });
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = findViewById(R.id.inputText);
                message = text.getText().toString().getBytes();

//                for (int l = 0; l < 5; l++) {
                // Original text
//                    switch (encryptSymbols[l]) {
//                        case '0': {

                byte[] encodedBytes = null;
                try {
                    byte[] decodedKey = privateKeyRSA.getBytes();
                    SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "RSA");
                    Cipher c = Cipher.getInstance("RSA");
                    c.init(Cipher.ENCRYPT_MODE, originalKey);
                    encodedBytes = c.doFinal(message);
                    message = encodedBytes;
                } catch (Exception e) {
                    Log.e("Crypto", "RSA encryption error");
                }
                handleSendClick(0);
                handleSendClick(1);
            }
            });
        profileSpinner = (Spinner) findViewById(R.id.profile);
        setupProfileSpinner();
        setupTransmitter();
    }
    public String RSAKey(String key){
        Key publicKey = null;
        Key privateKey = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            publicKey = kp.getPublic();
            privateKey = kp.getPrivate();
        } catch (Exception e) {
            Log.e("Crypto", "RSA key pair error");
        }
        if(key=="public")
        return publicKey.toString();
        else return privateKey.toString();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (transmitter != null) {
            transmitter.close();
        }
    }
    private void setupTransmitter() {
        FrameTransmitterConfig transmitterConfig;
        try {
            transmitterConfig = new FrameTransmitterConfig(
                    this,getProfile());
            transmitter = new FrameTransmitter(transmitterConfig);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ModemException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleSendClick(int i) {
        if (transmitter == null) {
            setupTransmitter();
        }
        send(i);
    }

    private void send(int l) {
        String payload;
        TextView text1=findViewById(R.id.textPublicKey);
        TextView text2=findViewById(R.id.textViewEncoded);
        payload=message.toString();
        String itog=publicKeyRSA+payload;
            try {
                transmitter.send(itog.getBytes());
                text1.setText(publicKeyRSA);
                text2.setText(payload);

            } catch (IOException e) {
                // our message might be too long or the transmit queue full
//            }
        }

    }

    private void setupProfileSpinner() {
        spinnerArrayAdapter = ProfilesHelper.createArrayAdapter(this);
        profileSpinner.setAdapter(spinnerArrayAdapter);
        profileSpinner.setSelection(0, false);
        profileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                transmitter = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private String getProfile() {
        String profile = spinnerArrayAdapter.getItem(profileSpinner.getSelectedItemPosition());
        return profile;
    }




    }
