package com.example.encryption;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class Get extends AppCompatActivity {
    private Spinner profileSpinner;
    private ArrayAdapter<String> spinnerArrayAdapter;
    private byte[] publicKey = null;
    private PublicKey originalKey = null;
    private byte[] decodedBytes = null;
    private Subscription frameSubscription = Subscriptions.empty();
    private byte[] encodedBytes = null;//Получаем байт код сообщения
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get);
        profileSpinner = (Spinner) findViewById(R.id.profile);
        setupProfileSpinner();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    subscribeToFrames();
                } else {
                    showMissingAudioPermissionToast();
                    finish();
                }
            }
        }
    }


    private void setupReceiver() {
        if (hasRecordAudioPersmission()) {
            subscribeToFrames();
        } else {
            requestPermission();
        };
    }

    private void subscribeToFrames() {
                frameSubscription.unsubscribe();
                frameSubscription = FrameReceiverObservable.create(this, getProfile()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(buf -> {
                    byte[] message = buf;
                    int lenght = message.length;
                    publicKey = new byte[1024-1];
                    System.arraycopy(1024, 1, publicKey, 0, 1024 - 1);
                    encodedBytes=new byte[lenght-1025];
                    System.arraycopy(lenght, 1025, encodedBytes, 1024, lenght - 1025);
                    try {
                        originalKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKey));
                    } catch (Exception e) {
                        Log.e("Crypto", "RSA decryption error");
                    }
                    TextView keyTextView = (TextView) findViewById(R.id.getKey);
                    keyTextView.setText("Key:\n" + originalKey.toString() + "\n");
                    try {
                    Cipher c = Cipher.getInstance("RSA");
                    c.init(Cipher.DECRYPT_MODE, originalKey);
                    decodedBytes = c.doFinal(encodedBytes);
                     } catch (Exception e) {
                     Log.e("Crypto", "RSA decryption error");
                    }
                    TextView decodedTextView = (TextView) findViewById(R.id.get);
                    decodedTextView.setText("[DECODED]:\n" + decodedBytes.toString() + "\n");
                });

            }
    boolean hasRecordAudioPersmission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO},
                MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
    }

    private void showMissingAudioPermissionToast() {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, R.string.missing_audio_permission, duration);
        toast.show();
    }

    private void setupProfileSpinner() {
        spinnerArrayAdapter = ProfilesHelper.createArrayAdapter(this);
        profileSpinner.setAdapter(spinnerArrayAdapter);
        profileSpinner.setSelection(0, false);
        profileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setupReceiver();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private String getProfile() {
        String profile = spinnerArrayAdapter.getItem(profileSpinner.getSelectedItemPosition());
        return profile;
    }
}
